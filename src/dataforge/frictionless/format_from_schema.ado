prog def _create_value_label
    version 17
    syntax name, enum(string asis) [mv_keep(string)]

    loc code 1
    foreach lab of loc enum {
        lab def `namelist' `code++' `"`lab'"', add
    }

    loc code 97
    foreach lab of loc mv_keep {
        lab def `namelist' .`=char(`code++')' `"`lab'"', add
    }
end

prog def _strip_mv
    version 17
    syntax varlist(max=1 string) [, mv_ignore(string)]
    foreach mval of loc mv_ignore {
        replace `varlist' = "" if `varlist'==`"`mval'"'
    }
end

prog def _replace_from_lab
    version 17
    syntax varlist(string max=1), label(string)

    matalabel `label', gen(n v l)
    mata: st_local("vals", invtokens(strofreal(v)'))
    foreach val of loc vals {
        replace `varlist' = string(`val') ///
            if `varlist'==`"`:label `label' `val''"'
    }
end

prog def _encode_or_label
    version 17
    syntax varlist(string max=1), type(string) format(string) label(string) ///
                                  [float]

    tempvar newvar
    if inlist("`type'", "string") {
        encode `varlist', lab(`label') gen(`newvar') noextend
        order `newvar', after(`varlist')
        drop `varlist'
        ren `newvar' `varlist'
    }
    else if inlist("`type'", "number", "integer", "year") {
        _replace_from_lab `varlist', label(`label')
        destring `varlist', replace `float'
        lab val `varlist' `label'
    }
    else if inlist("`type'", "boolean") {
        _replace_from_lab `varlist', label(`label')
        replace `varlist' = "0" if lower(`varlist') == "false"
        replace `varlist' = "1" if lower(`varlist') == "true"
        destring `varlist', replace
        lab val `varlist' `label'
    }
    else if inlist("`type'", "date") {
        _replace_from_lab `varlist', label(`label')
        replace `varlist' = string(date(`varlist', "YMD")) ///
                            if !mi(date(`varlist', "YMD"))
        destring `varlist', replace
        format `varlist' %tdCCYY-NN-DD
        lab val `varlist' `label'
    }
    else if inlist("`type'", "yearmonth") {
        _replace_from_lab `varlist', label(`label')
        replace `varlist' = string(monthly(`varlist', "YM")) ///
                            if !mi(monthly(`varlist', "YM"))
        destring `varlist', replace
        format `varlist' %tmCCYY-NN
        lab val `varlist' `label'
    }

    // TODO Handle remaining types

end

prog def format_from_schema
    version 17
    syntax varlist(max=1 string) ///
        [, type(string) format(string) label(string) enum(string asis) ///
           missing_vals(string asis) title(string) description(string) ///
           vl_from_enum mv_ignore(string asis) float]

    loc varname "`varlist'"

    if !mi("`label'") {
        matalabel `label', gen(n v l)
        mata: st_local("labels", invtokens((("`"+char(34)):+l:+(char(34)+"'"))'))
        loc mv_ignore: list missing_vals - labels
        _strip_mv `varname', mv_ignore(`mv_ignore')
        _encode_or_label `varname', type(`type') format(`"`format'"') ///
            label(`label')
    }
    else if inlist("`type'", "string") & !mi(`"`enum'"', "`vl_from_enum'") {
        loc mv_keep: list missing_vals - mv_ignore
        _create_value_label `varname', enum(`enum') mv_keep(`mv_keep')
        _strip_mv `varname', mv_ignore(`mv_ignore')
        _encode_or_label `varname', type(`type') format(`"`format'"') ///
            label(`varname')
    }
    else if inlist("`type'", "number", "integer", "year") {
        _strip_mv `varname', mv_ignore(`missing_vals')
        destring `varname', replace `float'
    }
    else if inlist("`type'", "boolean") {
        _strip_mv `varname', mv_ignore(`missing_vals')
        replace `varname' = "0" if lower(`varname') == "false"
        replace `varname' = "1" if lower(`varname') == "true"
        destring `varname', replace
    }
    else if inlist("`type'", "date") {
        _strip_mv `varname', mv_ignore(`missing_vals')
        replace `varname' = string(date(`varname', "YMD")) ///
                            if !mi(date(`varname', "YMD"))
        destring `varname', replace
        format `varname' %tdCCYY-NN-DD
    }
    else if inlist("`type'", "yearmonth") {
        _strip_mv `varname', mv_ignore(`missing_vals')
        replace `varname' = string(monthly(`varname', "YM")) ///
                            if !mi(monthly(`varname', "YM"))
        destring `varname', replace
        format `varname' %tmCCYY-NN
    }

    // TODO Handle remaining types
    // TODO Consider option to construct value labels automatically from
    //      missing values for numeric vars

    if !mi(`"`title'"') lab var `varname' `"`title'"'
    if !mi(`"`description'"') note `varname': `description'
end
